import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { NewsComponent } from './components/news/news.component';
import { HomeComponent } from './components/home/home.component';
import { FormComponent } from './components/form/form.component';
import { SearchComponent } from './components/search/search.component';
import { TodolistComponent } from './components/todolist/todolist.component';
import { News2Component } from './components/news2/news2.component';
import { HeaderComponent } from './components/header/header.component';
import { TransitionComponent } from './components/transition/transition.component';
import { AnimationComponent } from './components/animation/animation.component';
import { Header2Component } from './components/header2/header2.component';
import { Home2Component } from './components/home2/home2.component';
import { RxjsTestComponent } from './components/rxjs-test/rxjs-test.component';


const routes: Routes = [
  { path: 'app-header', component: HeaderComponent },
  { path: 'app-news', component: NewsComponent },
  { path: 'app-form', component: FormComponent },
  { path: 'app-search', component: SearchComponent },
  { path: 'app-todolist', component: TodolistComponent },
  { path: 'app-home', component: HomeComponent },
  { path: 'app-news2', component: News2Component },
  { path: 'app-transition', component: TransitionComponent },
  { path: 'app-animation', component: AnimationComponent },
  { path: 'app-home2', component: Home2Component },
  { path: 'app-rxjs-test', component: RxjsTestComponent },
  { path: '',   redirectTo: '/app-news', pathMatch: 'full' },
  { path: '**', component: NewsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { enableTracing: true } // <-- debugging purposes only
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
