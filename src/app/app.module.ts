import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NewsComponent } from './components/news/news.component';
import { HomeComponent } from './components/home/home.component';
import { FormComponent } from './components/form/form.component';
import { SearchComponent } from './components/search/search.component';
import { TodolistComponent } from './components/todolist/todolist.component';
import { News2Component } from './components/news2/news2.component';
import { HeaderComponent } from './components/header/header.component';
import { TransitionComponent } from './components/transition/transition.component';
import { AnimationComponent } from './components/animation/animation.component';
import { Home2Component } from './components/home2/home2.component';
import { Header2Component } from './components/header2/header2.component';
import { FooterComponent } from './components/footer/footer.component';
import { New3Component } from './components/new3/new3.component';
import { RxjsTestComponent } from './components/rxjs-test/rxjs-test.component';

//引入并且配置服务
import { StorageService } from './services/storage.service';
import { Storage2Service } from './services/storage2.service';
import {HttpClientModule,HttpClientJsonpModule} from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    NewsComponent,
    HomeComponent,
    FormComponent,
    SearchComponent,
    TodolistComponent,
    News2Component,
    HeaderComponent,
    TransitionComponent,
    AnimationComponent,
    Home2Component,
    Header2Component,
    FooterComponent,
    New3Component,
    RxjsTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers: [StorageService,Storage2Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
