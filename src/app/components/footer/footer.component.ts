import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Output() public outer=new EventEmitter();

  public msg = '我是子组件footer的msg';

  constructor() { }

  ngOnInit(): void {
  }

  run(){
    alert('我是子组件的run方法');
  }

  sendParent(){
    this.outer.emit('我是子组件的数据');
  }

}
