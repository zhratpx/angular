import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.scss']
})
export class Header2Component implements OnInit {

  @Input() title:any;
  @Input() msg:any;
  @Input() run:any;

  @Input() home:any;

  constructor() { }

  ngOnInit(): void {
  }

  getMsg(){
    alert(this.msg);
  }

  getRun(){
    this.run();
  }

  getAll(){
    alert(this.home.msg)
    this.home.run();
  }
}
