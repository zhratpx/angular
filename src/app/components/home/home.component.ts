import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  public flag:boolean=true;

  //获取不到dom节点
  ngOnInit(): void {
    // let oBox1:any=document.getElementById("box1");
    // oBox1.style.color="blue";
  }


  //angular生命周期函数，试图加载完成之后触发的方法 dom加载完成
  ngAfterViewInit(){
    let oBox1:any=document.getElementById("box1");
    oBox1.style.color="blue";
  }

  


}
