import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss']
})
export class Home2Component implements OnInit {

  public title:String="首页组件的标题";

  public msg:String="我是父组件的msg";

  constructor() { }

  ngOnInit(): void {
  }
  
  run(){
    alert('执行了父组件的run方法')
  }

}
