import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-new3',
  templateUrl: './new3.component.html',
  styleUrls: ['./new3.component.scss']
})
export class New3Component implements OnInit {

  @ViewChild('footer') footer:any;

  constructor() { }

  ngOnInit(): void {
  }

  getChildMsg(){
    alert(this.footer.msg);
  }
  getChildRun(){
    this.footer.run();
  }
  run(){
    alert('我是父组件的run方法');
  }
}
