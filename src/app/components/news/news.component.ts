import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  title="绑定数据title"

  username:string="zhangsan";

  public userInfo:any={
    username:'zhangsansan',
    age:20
  }

  public student:string="wushiyigexuesheng";

  public message:any;

  public content:string="<h2>zheshiyigeH2</h2>"

  public arr:any[] = ['11111','2222','3333'];

  public userArr:any[] = [{
    name:'zhangsan',
    age:'20'
  },{
    name:'zhangsan1',
    age:'201'
  },{
    name:'zhangsan2',
    age:'202'
  }]

  public cars:any[] = [{
    name:'宝马',
    list:[{
      title:"宝马5",
      price:'60W'
    }]
  },{
    name:'奥迪',
    list:[{
      title:"奥迪Q1",
      price:'60W'
    },{
      title:"奥迪Q2",
      price:'601W'
    },{
      title:"奥迪Q3",
      price:'602W'
    }]
  },{
    name:'奔驰',
    list:[{
      title:"奔驰S",
      price:'100W'
    },{
      title:"奔驰S1",
      price:'1001W'
    },{
      title:"奔驰S2",
      price:'1002W'
    }]
  },]

  constructor() { 
    this.message='zheshigaibianshuxingdexzhi';
    this.username='gaibian';
  }

  ngOnInit(): void {
  }

}
