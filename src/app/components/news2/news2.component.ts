
/*
  1、模板中给dom起一个名字
  2、在业务逻辑里面引入viewChild
  3、写在类里面  @ViewChild('myBox') myBox:any;
  4、在生命周期函数 ngAfterViewInit() 获取dom
 */



import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-news2',
  templateUrl: './news2.component.html',
  styleUrls: ['./news2.component.scss']
})
export class News2Component implements OnInit {

  @ViewChild('myBox') myBox:any;
  @ViewChild('header') header:any;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
    this.myBox.nativeElement.style.width="100px";
    this.myBox.nativeElement.style.height="100px";
    this.myBox.nativeElement.style.background="red";

  
    
  }

  getHeaderRun(){
    this.header.run();
  }

}
