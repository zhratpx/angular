import { Component, OnInit } from '@angular/core';
import { Storage2Service } from '../../services/storage2.service';

@Component({
  selector: 'app-rxjs-test',
  templateUrl: './rxjs-test.component.html',
  styleUrls: ['./rxjs-test.component.scss']
})
export class RxjsTestComponent implements OnInit {
  public datalist:any[]=[]

  constructor(public storage:Storage2Service) { }

  ngOnInit(): void {
  }
  onsearch(){
    // this.storage.getData();
    // console.log(this.storage.getData(),2000)
   var rxinterval= this.storage.getRxjsData()
   rxinterval.subscribe((data)=>{
     this.datalist=data;
     console.log(data,3000)

   })
  }

}
