import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(public storage:StorageService) { }

  public keyword:String="";
  public historyList:any[]=[];

  ngOnInit(): void {

    let searchlist = this.storage.get("searchlist");
    if(searchlist){
      this.historyList = searchlist;
    }

  }

  doSearch(){
    if(this.historyList.indexOf(this.keyword)==-1){
      this.historyList.push(this.keyword);

      this.storage.set("searchlist",this.historyList);
    }
    
    this.keyword="";
  }

  deleteHistory(key:any){
    this.historyList.splice(key,1);
    this.storage.set("searchlist",this.historyList);
  }

}
