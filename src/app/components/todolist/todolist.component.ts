import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {

  public keyword:string;

  public todolist:any[]=[];


  constructor(public storage:StorageService) { }

  ngOnInit(): void {

    let todolist = this.storage.get("todolist");
    if(todolist){
      this.todolist = todolist;
    }
  }

  doAdd(e:any){
    if(e.keyCode==13){
      if(!this.todolistHaskeyword(this.todolist,this.keyword)){
!        this.todolist.push({
          title:this.keyword,
          status:0
        });
      this.keyword="";


      this.storage.set("todolist",this.todolist);
      }else{
        alert("数据已经存在！");
        this.keyword="";
      }
     

    }

  }

  deleData(key:any){
  
    this.todolist.splice(key,1);
    this.storage.set("todolist",this.todolist);
    
  }

  todolistHaskeyword(todolist:any,keyword:any){

    // todolist.forEach(value => {
    //   if(value.title==keyword){
    //     return true;
    //   }
      
    // });

    if(!keyword)
      return false;
    

    for(var i=0;i<todolist.length;i++){
      if(todolist[i].title==keyword){
        return true;
      }

    }

  }

  checkboxChange(){
    this.storage.set("todolist",this.todolist);
  }

}
