import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transition',
  templateUrl: './transition.component.html',
  styleUrls: ['./transition.component.scss']
})
export class TransitionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  showAside(){
    let asideDmo:any=document.getElementById('aside');
    asideDmo.style.transform="translate(0,0)";
  }
  hideAside(){
    let asideDmo:any=document.getElementById('aside');
    asideDmo.style.transform="translate(100%,0)";
  }

}
