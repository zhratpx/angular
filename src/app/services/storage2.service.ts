import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Storage2Service {
  public url:any ='https://liugaochao.top/getImages.json'
  public datalist:any[]=[]
  constructor(public http:HttpClient) { }

  getData(){
    this.http.get('/api/getImages.json').subscribe((response:any) =>{
      this.datalist=response.data
      console.log(this.datalist,1000)
      //return this.datalist
    })
  }

  getRxjsData(){
    this.getData()
    return new Observable<any>((observer)=>{
      setInterval(()=>{
        observer.next(this.datalist)
      },1000);
    })

  }
}
